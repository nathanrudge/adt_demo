/*********************/
/** STANDARD ASSETS **/
/*********************/
For demo scene to work properly you will need to import "Characters" and "Environment" from Standard Assets. 
You can do this by right clicking inside the Project view in the editor > Import Package > Package name.
If you do not want to use the demo scene you do not need to import dany standard assets.

[Optional] 
Demo scene uses "Post Processing Stack" availabe for free from Unity Technologies on the Unity Asset Store.
Just import the package if you want the same demo look.



/****************************/
/** INPUT MANAGER SETTINGS **/
/****************************/
If you want to use scene settings from the demo or just want the set-up input manager, extract the wanted contents of ./ProjectSettings.zip 
into your ProjectSettings directory ([your_project_name]/ProjectSettings).
This will overwrite your existing settings.